import csv
from functools import reduce

import jinja2


def foo(xs):
    def baz(acc, x):
        plating = x['plating'] or acc[0]
        if x['public']:
            x['plating'] = plating
            return (None, acc[1] + [x])
        else:
            return (plating, acc[1])

    return reduce(baz, xs, (None, []))[1]

def bar(line):
    # import pdb; pdb.set_trace()
    [guide_id, honor, active, imie, nazwisko, show_name, email, show_email, telefon, show_telefon, uwagi, uprawnienia, show_uprawnienia, dead, where, year] = line
    no = guide_id if int(guide_id) > 0 else '-'
    dead = dead != 'NULL' and int(dead)
    name = '{0} {1}{2}'.format(imie.strip(), nazwisko.strip(), ' †' if dead else '')
    public = dead or int(show_name)
    email = email if int(show_email) and email and email != 'NULL' else None
    phone = telefon if int(show_telefon) and telefon and telefon != 'NULL' else None
    plating = '{0} {1}'.format(where.strip(), year.strip()).strip() if where else None
    details = uprawnienia if int(show_uprawnienia) and uprawnienia and uprawnienia != 'NULL' else None

    return dict(no=no, name=name, email=email, phone=phone, details=details, plating=plating or None, public=public)

# SELECT * FROM www_guides AS g INNER JOIN www_members AS m JOIN www_platings AS p WHERE g.member_id = m.member_id AND g.plating_id = p.plating_id ORDER BY g.guide_id LIMIT 5000
with open('przewodnicy - przewodnicy.csv', 'r') as f:
    xs = list(csv.reader(f))

INDEX = 2

columns = xs[0][:INDEX]
columns = ['#', 'Imię i nazwisko', 'Adres e-mail', 'Nr telefonu']
rows = foo(map(bar, xs[1:]))

with open('przewodnicy.html', 'r') as f:
    print(jinja2.Template(f.read()).render(columns=columns, rows=rows))
