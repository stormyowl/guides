# guides

Python script for generating guides HTML table

To generate guides list:
* install requirements running `pip install -r requirements.txt` - it's best to install them in separate virtualenv,
* download file from Google Drive, save it to this directory as a CSV file named `przewodnicy - przewodnicy.csv`,
* run `python3 przewodnicy.py`,
* the output in the terminal is the HTML table containing all guides that gave the consent to publish their data.
