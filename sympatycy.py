import csv
import jinja2
import re

def bar(line):
    [year, plate_no, imie, nazwisko, show_name, email, show_email, telefon, show_telefon, dead] = line[:10]
    plate_no = '-' if plate_no == 'bn' else plate_no
    name = '{0} {1}{2}'.format(imie.strip(), nazwisko.strip(), ' †' if int(dead) else '')
    public = int(dead) or int(show_name)
    year = year.strip() if year and year != '-' else None

    return dict(name=name, year=year, public=public)


# SELECT * FROM www_sympathizers AS s INNER JOIN www_members AS m WHERE s.member_id = m.member_id ORDER BY s.sympathizer_id LIMIT 5000
with open('sympatycy - sympatycy.csv', 'r') as f:
    xs = list(csv.reader(f))

INDEX = 2

year_regexp = '^(\d{4})(\??)((-|\/)\d{4})?$'

def get_year(value):
    if value is None:
        return 1975
    
    if (re.match(year_regexp, value.strip()) is None):
        print(value)
    
    return int(re.match(year_regexp, value.strip()).group(1))
        
columns = xs[0][:INDEX]
columns = ['Imię i nazwisko', 'Rok']
rows = sorted(filter(lambda x: x['public'], map(bar, xs[1:])), key=lambda x: get_year(x['year']))

with open('sympatycy.html', 'r') as f:
    print(jinja2.Template(f.read()).render(columns=columns, rows=rows))
